
#create db with initial datacreate database phone;

  create database phone;
  use phone;

  create table numbers (
    name varchar(50),
    number varchar(30)
  );
  insert into numbers values('Steve','555-1234');
  insert into numbers values('KP','335-3331');
  insert into numbers values('Will','788-3331');
